import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Camera } from '@ionic-native/camera';
import { ImagePage } from '../pages/image/image';
import { AngularDraggableModule } from 'angular2-draggable';
import { ImageEditPage } from '../pages/image-edit/image-edit';
import { ImageSharePage } from '../pages/image-share/image-share';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ImagePage,
    ImageEditPage,
    ImageSharePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      swipeBackEnabled: false 
    }),
    AngularDraggableModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ImagePage,
    ImageEditPage,
    ImageSharePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
