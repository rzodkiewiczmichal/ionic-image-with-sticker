import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePage } from '../image/image';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private camera: Camera,
     private sanitizer: DomSanitizer, private platform: Platform) {

  }

  openCamera(): void {
    if(this.platform.is('cordova')){
      console.log('camera')
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      
      this.camera.getPicture(options).then((imageData) => {
       // imageData is either a base64 encoded string or a file URI
       // If it's base64 (DATA_URL):
       let base64Image = 'data:image/jpeg;base64,' + imageData;
       this.navCtrl.push(ImagePage, this.sanitizer.bypassSecurityTrustResourceUrl(base64Image));
      }, (err) => {
       // Handle error
      });
    }else{
      this.navCtrl.push(ImagePage, this.sanitizer.bypassSecurityTrustResourceUrl('assets/imgs/mock-image.jpg'));
    }

  }

}
