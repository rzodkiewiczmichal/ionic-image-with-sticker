import { Component, ViewChild } from "@angular/core";
import { NavParams, NavController } from "ionic-angular";
import domtoimage from 'dom-to-image';
import { DomSanitizer } from "@angular/platform-browser";
import { ImagePage } from "../image/image";
import { ImageSharePage } from "../image-share/image-share";
import { c } from "@angular/core/src/render3";


@Component({
    templateUrl: 'image-edit.html',
    selector: 'image-edit-page'
})
export class ImageEditPage{
    private image; 


    constructor(private navParams: NavParams, private nav: NavController,
         private sanitizer: DomSanitizer){

    }

    ionViewWillEnter(){
        this.image = this.navParams.data;
        console.log(this.image);
    }

    cancel(){
        this.nav.pop();
    }

    save(){
        let node = document.getElementById('imageNode');
        console.log(this.image);
        domtoimage.toSvg(node, {height: this.image.height, width: this.image.width})
            .then((dataUrl) => {
                this.nav.push(ImageSharePage, this.sanitizer.bypassSecurityTrustResourceUrl(dataUrl));
            })
            .catch((error) => {
                console.log('error');
                console.log(error);
            })
    }
}