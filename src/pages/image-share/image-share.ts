import { Component } from "@angular/core";
import { NavParams, NavController } from "ionic-angular";

@Component({
    templateUrl: 'image-share.html',
    selector: 'image-share'
})
export class ImageSharePage{
    private imageData;

    constructor(private navParams: NavParams, private nav: NavController) {}

    ionViewWillEnter(){
        this.imageData = this.navParams.data;
    }

    facebook(){}

    instagram(){}

    more(){}

    exit(){
        this.nav.goToRoot({});
    }

}