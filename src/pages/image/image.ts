import { Component, ViewChild } from "@angular/core";
import { NavParams, NavController } from "ionic-angular";
import { ImageEditPage } from "../image-edit/image-edit";

@Component({
    templateUrl: 'image.html',
    selector: 'image-page'
})
export class ImagePage{

    @ViewChild('image') image;
    private imageData; 

    constructor(private navParams: NavParams, private nav: NavController){

    }

    ionViewWillEnter(){
        this.imageData = this.navParams.data;
        console.log(this.image);
    }

    cancel(){
        this.nav.pop();
    }

    addSticker(){
        this.nav.push(ImageEditPage, 
            {
                imageData: this.imageData,
                height: this.image.nativeElement.height,// * 0.6,
                width: this.image.nativeElement.width// * 0.6
            });
    }
}